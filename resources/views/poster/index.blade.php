<!DOCTYPE html>
<html>
    <head>
            <meta charset="UTF-8" />
            <title>当該ユーザのカレンダー一覧</title>
            <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>

    <h1>新規カレンダーを追加</h1>
    {!! Form::open(['route' => 'poster.create' ]) !!}
    名称<input type="text" name="name"  ><br>
    <input type="text" name="year"  >年　<input type="text" name="month"  >月
    {!! Form::submit('年月で新規作成') !!}
    {!! Form::close() !!}
    <section id="table_list" >
        <table id = "calendar_list" >
            <tr><td> ID </td><td> 名称 </td><td> 年月 </td><td> 編集 </td><td> 削除 </td></tr>
         @foreach($posters as $p)
            <tr><td> {{ $p->id }} </td><td> {{ $p->name }} </td><td> {{ $p->year }} / {{ $p->month }} </td>
            <td>
                <a href="{{ URL::route('poster.edit',array('id' => $p->id )) }}">
                編集
                </a>
            </td>
            <td>
                {!! Form::open(['route' => 'poster.delete' ]) !!}
                  <input type="hidden" name="id"  value="{{ $p->id }}">
                {!! Form::submit('削除') !!}
                {!! Form::close() !!}
                </a>
            </td>
            </tr>
         @endforeach
        </table>
    </section>
    </body>
</html>
