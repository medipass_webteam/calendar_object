<!DOCTYPE html>
<html>
    <head>
            <meta charset="UTF-8" />
            <title>カレンダー要素を　{{ $conf["title"] }}</title>
            <link rel="stylesheet" type="text/css" href="{{ $conf['root_path'] }}/assets/css/calendar_mod.css"/>
            <link rel="stylesheet" type="text/css" href="{{ $conf['root_path'] }}/assets/js/jquery-ui-1.12.1/jquery-ui.min.css"/>
            <script src="{{ $conf['root_path'] }}/assets/js/jquery-3.1.1.min.js"></script>
            <script src="{{ $conf['root_path'] }}/assets/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
            <!-- 例によって、JQUERYやプラグインを読み込んだ後で読み込むこと！！ -->
            <script src="{{ $conf['root_path'] }}/assets/js/calendar_mod.js"></script>
    </head>
    <body>
    <h1>指定した枠内でのみ、自由に動かせるようにする。PCまたはタブレットで操作を想定する</h1>
    <h2>保存した予定をajaxで受け取るなどの、サーバ側操作も必要だがここでは割愛</h2>
     <P> {{ $poster[0]->year }} 年　 {{ $poster[0]->month }} 月</P>
    <section id="container" >
        <div id = "events" >
        </div>

        <table id = "calendar" >
            <tr class="header"><td>日</td><td>月</td><td>火</td><td>水</td><td>木</td><td>金</td><td>土</td></tr>
            <?php echo $conf["calendar"]["dom"]; ?>
        </table>
    </section>


    <h2 id = "edit_head" >文字オブジェクトを追加する</h2>
    <p>タイトル<input id="desc" name="desc" type="text" ></p>
    <p>背景イメージ </p>
    <p>
    <?php foreach( $conf["stamp_list"] as  $l ){ ?>
        <img id="icon_{{ $l['id'] }}" class="stamp_list" src="{{ $conf['root_path'] }}{{ $l['path'] }}" value="{{ $l['path'] }}" ></img>
    <?php } ?>
    </p>
    <p id="add_obj" >投稿する</p>

    <section id="event_list" >
        <table id = "calendar_list" >
            <tr><td> ID </td><td> 名称 </td><td> イメージ </td></tr>
        </table>
    </section>
    </body>
</html>
