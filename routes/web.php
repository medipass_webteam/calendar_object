<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/poster', array( 'as' => 'poster.index','uses' => 'PosterController@index' ) );
Route::post('/poster/create', array( 'as' => 'poster.create','uses' => 'PosterController@create' ) );//年月指定し新規作成
Route::get('/poster/edit/{id}', array( 'as' => 'poster.edit','uses' => 'PosterController@edit' ) );
Route::post('/poster/delete', array( 'as' => 'poster.delete','uses' => 'PosterController@delete' ) );
