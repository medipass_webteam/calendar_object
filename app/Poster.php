<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poster extends Model
{

    /*使用可能なスタンプ一覧を用意する 戻り値として、有効なスタンプリストのIDとパスを保存した連想配列を返す*/
   public static function stampList(){


      $stamp_files = array("sample0.png","sample1.png","sample2.png");
      $stamp_list = array();

      foreach( $stamp_files as $id => $name ){
        $path = \Config::get('const.ROOT_PATH')."/assets/images/stamps/".$name;
        $stamp_list[] = array('id' => $id ,'path' => $path );
      }

      return $stamp_list;
   }

   /*年月に対応するカレンダー背景のhtmlを生成する*/
   public static function plane_calendar_obj($year,$month){
      /*当月の末日を取得する。*/
      $ym = $year."-".$month;
      $last_date = (integer)date('t',strtotime($ym));
      $day_count =array(0,0,0,0,0,0,0);//曜日総数カウント
      $row_count = 1;//週が変わった数

      $first_date = $year."-".$month."-1";
      echo $first_date;
      $first_day_week = (integer)date("w",strtotime($first_date));


      /*もし、初日が日曜日以外なら
        8マイナス曜日パターンで最初の日曜日がもとめられる。
        初日が日曜日なら、１日をそのまま入れる。
      */
      if ( $first_day_week !== 0 ){
          $first_sunday = 8 - $first_day_week;
          $total_rows = (integer)ceil( ($last_date - $first_sunday + 1 ) / 7 ) + 1;
      }else{
          $first_sunday = 1;
          $total_rows = (integer)ceil( ($last_date - $first_sunday + 1 ) / 7 );
      }


      /*何曜日から始まるか？週はいくつあるか？*/
      $ret = array();
      $ret["first_day_week"] = $first_day_week;
      $ret["first_sunday"] = $first_sunday;
      $ret["total_days"] = $last_date;
      $ret["total_rows"] = $total_rows;

      var_dump($ret);

      $ret["dom"] = Poster::plane_calendar_dom($ret);

      return $ret;

      //echo $last_date;
   }

   /*週部分のDOMを生成する
      引数　月初の曜日、最初の日曜日、合計日数、合計週数
   */
   public static function plane_calendar_dom($arg){

      $tmp = "";
      /*最初の、曜日の存在する行を用意する。日曜日以外なら前側に余白を作る*/
      if ( $arg["first_day_week"] !== 0){
        $tmp = "<tr><td class=\"empty_cell\" colspan=\"".$arg["first_day_week"]."\"></td>";
      }else{
        $tmp = "<tr>";
      }

      $week_day = $arg["first_day_week"];
      /*次に、１日から最終日まで詰めていく*/
      for( $i = 1; $i <= $arg["total_days"]; $i ++ ){
        $tmp .= "<td>".$i."</td>";
        /*もし土曜日の次なら、改行して日曜日とする*/
        $week_day ++;
        if ( $week_day > 6 ) {
          $tmp .="</tr>";
          //続きがあれば、次の行を用意する
          if ( $i < $arg["total_days"] ){
             $tmp .="<tr>";
             $week_day = 0;
          }

        }

      }

      /*最終日より後ろの枠を生成する。*/
      if ( $week_day <= 6 ){
        $rest_days = 7 - $week_day;//残りのます数
        $tmp .= "<td class=\"empty_cell\" colspan=\"".$rest_days."\"></td></tr>";
      }

      /*合計何週間かで、処理する*/
      $empty_week = '<tr><td class="empty_cell" colspan ="7" ></td></tr>';

      if (  $arg["total_rows"] == 4 ){
        return $empty_week.$tmp.$empty_week;
      }elseif( $arg["total_rows"] == 5 ) {
        return $empty_week.$tmp;
      }else{
        return $tmp;
      }

   }
}
