<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request; // これだと non-static method
use Request;

use Response; // ←追加
use Input; // ←追加

use App\Poster;

//use App\TeacherTag;
//use App\TeacherTeacherTag;

use Illuminate\Database\Eloquent\Model;

use DB;//ファザード導入


class PosterController extends Controller
{
    //一覧表示
    public function index(){
      /*ポスターのリスト*/
      //$posters = DB::table('posters')->where('user_id','=',100)->get();
      $posters = DB::table('posters')->get();

      return view('Poster.index',compact('posters'));
    }

    //この手のフレームワークの構造から外れるが、新しい予定を作る処理
    public function create(){
      //var_dump(Request::all());
      $rec = array();
      $rec["name"] = Request::input('name');
      $rec["year"] = Request::input('year');
      $rec["month"] = Request::input('month');

      /*本来、この辺にバリデーション処理を入れる*/
      $poster = new Poster;
      $poster->user_id = 0;
      $poster->name = $rec["name"];
      $poster->year = $rec["year"];
      $poster->month = $rec["month"];

      $poster->save();

      //スタンプリストを取得
      $stamp_list = Poster::stampList();

      /*設定*/
      $conf["title"] = "指定年月の予定を新規作成";

      //return view('Poster.edit',compact('stamp_list','conf'));
    }

    /*IDをうけとり、あれば検索*/
    public function edit($id){
      $rec = array();
      $rec["id"] = $id;

      /*指定IDかつユーザIDが一致するものを取得する。*/
      $poster = DB::table('posters')
      ->where('id',$id)
      ->where('user_id',0)
      ->get();

      /*カウントする。１件の時のみが正しい。*/
      $poster_count = $poster->count();

      if ( $poster_count > 0 ){
        // $objects = DB::table('poster_objects')
        // ->where('poster_id',$id)
        // ->get();

        /*設定*/
        $conf["title"] = "予定情報の編集";
        $conf["stamp_list"] = Poster::stampList();
        $conf["root_path"] = \Config::get('const.root_path');
        $conf["calendar"] = Poster::plane_calendar_obj($poster[0]->year,$poster[0]->month);

        //var_dump( \Config::get('const.root_path') );
        return view('Poster.edit',compact('poster','conf'));

      }


      /*関連レコードを算出する*/
      echo "no exist ";
    }

    //IDを受け取り、そのIDの項目を削除する
    public function delete(){

    }
    /*IDを受け取り、そのIDに相当するものがあれば返す*/
    public function fetch_checked(){

    }

    /*ここから先、頻繁に使用するプライベートなメソッド*/




}
