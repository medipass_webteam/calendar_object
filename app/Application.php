<?php

namespace App;

/*アプリケーションで、新しいドキュメントルートを指定する。*/
class Application extends \Illuminate\Foundation\Application
{
    public function publicPath()
    {
      return '/var/www/html/calendar_api_public';
    }
}
