<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posters', function (Blueprint $table) {
            //最低限　シリアルID　年、月があれば良い
            $table->bigIncrements('id')->index();
            $table->unsignedBigInteger('user_id');
            $table->string('name',128);
            $table->unsignedSmallInteger('year');
            $table->unsignedSmallInteger('month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('posters');
    }
}
