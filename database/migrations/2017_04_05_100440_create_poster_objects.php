<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosterObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poster_objects', function (Blueprint $table) {
            //最低限　シリアルID　年、月があれば良い
            $table->bigIncrements('id')->index();
            $table->unsignedBigInteger('poster_id');
            $table->string('title',128);//タイトルは１２８文字分あればいいか
            $table->text("stamp")->nullable();//背景として設定する画像
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('posters');
    }
}
